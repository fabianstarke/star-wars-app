## Install the app
npm install

## Run the app
npm run start

## Login with creditentials
email: user@test.com
password: 123456



## Project Structure

I use Material-ui library for this project

### App.js

- Use of an Auth context provider that keeps the auth state of the app. We spread it to our component tree in order to have access to it
- Use of react-router-dom for routing purposes, with the use of a PrivateRoute components which is protecting our home route
- We render the Header component, the login component on first connection. Once user logs in successfully he is redirected to the Dashboard component
- Use of a Data context provider to store the data coming from the Swapi Api. 
- We Map on the ListItem array that is declared above in order to render the List component passing dow to its props the array items


### Views 

#### Dashboard

Possibility for the user to search for a star wars ressource.
The user first selects a ressource. Through a Selector component we trigger the users choice and set it as the value.
A SearchBox appears and by typing we make a call to the correct ressource endpoint that has been selected before and we use debounce from lodash library in order to return only results when user finishes to type.
Cards appear corresponding to the user research. Here we use the OverviewCard component.
It is also possible to click on the card to get more information on the user sresearch. 

#### List 

We use this component in our `App.js` file as we map through an array and render this `List` component.
This component maps over some data that we get from the `DataContextProvider` we use a `fetch` prop to pass in the `api` `endpoint` corresponding to a star wars ressource.


### Login

This view uses the `Form` component and we set the value of 2 input fields `email` and `password`.

We submit the form using firebase function to check if there is a valid input of the fields. In case it is not the case an alert is prompted to the user.
We use the `user` state from the auth context in order to redirect to the protected route if there is an user.

## Components

#### Drawer

Simple Drawer that the user can open when logged in.
When clicking on a ressource it Links to the correct ressource endpoint that will load the ressource.

#### Form and FormTextField

Simple form component with some props


#### Header

Here we dertermine if the user is logged in through the `AuthContext`.
Depending on this we display rather a simple Header with a logo or, if the user is connected a menu icon to open up the drawer, and a signout icon.
To Signout we use firebase `signOut` function

### ItemDetails

When the users click on `more info` on a ressource Card a diolog open up containing some detailed informaton about the ressoruce.
We are passing some props here to get the data and we also use a `List and ListItem` components from material-ui.
We map over the `details` object to display the keys and values of it into the list.

#### Loader

Simple spinner that display while looking for a ressource

#### OverviewCard

This is the card that will render when user types in a research or when clicking on a ressource on the Drawer component.
We pass some props to display major information about the ressource

#### PrivateRoute

Wrapper around the regular routes It passes the rest of the props and than in our render function depending if there is a user or not we redirect to the component or initial route

#### SearchBox

Basic search box for user to type in query

#### Selector

Selector displaying menu itmes corresponding to a star wars ressource

### Contexts

We have 
 contexts: `AuthContext` that stores the user state and `DataContext` that makes calls to the `swapi` api depending on the ressource `type` we pass in

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
