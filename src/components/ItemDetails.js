/* eslint-disable react/prop-types */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DialogContent from '@material-ui/core/DialogContent';
import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import uuid from 'uuid/v4';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
    background: 'linear-gradient(0.3turn, black, blue, black)',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  list: {
    background: 'linear-gradient(0.3turn, black, blue, black)',
  },
  text: {
    color: '#fff',
  },
  primary: {
    color: '#fff',
  },
  divider: {
    backgroundColor: '#fff',
  },
}));
// eslint-disable-next-line react/display-name
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function ItemDetails(props) {
  const classes = useStyles();
  const { open, close, title, details } = props;
  return (
    <Dialog
      fullScreen
      open={open}
      onClose={close}
      TransitionComponent={Transition}
    >
      <AppBar className={classes.appBar}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={close}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            {title}
          </Typography>
        </Toolbar>
      </AppBar>
      <DialogContent className={classes.list}>
        <List>
          {/* Deliberally only display values if object values are string in order to avoid new calls to api */}
          {Object.entries(details).map(item => (
            <div key={uuid()}>
              <ListItem button>
                <ListItemText
                  classes={{
                    primary: classes.primary,
                    secondary: classes.text,
                  }}
                  primary={item[0]}
                  secondary={typeof item[1] === 'string' ? item[1] : null}
                />
              </ListItem>
              <Divider classes={{ root: classes.divider }} />
            </div>
          ))}
        </List>
      </DialogContent>
    </Dialog>
  );
}
