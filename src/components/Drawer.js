/* eslint-disable react/prop-types */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from 'react-router-dom';
import SearchIcon from '@material-ui/icons/Search';
import DeathStar from '../assets/DeathStar.svg';
import Falcon from '../assets/Falcon.svg';
import BB8 from '../assets/BB8.svg';
import Vehicle from '../assets/Vehicle.svg';
import People from '../assets/People.svg';
import DarthVader from '../assets/DarthVader.svg';

const drawerWidth = 180;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  menuButton: {
    marginRight: theme.spacing(2),
    color: 'yellow',
  },
  headerIcon: {
    color: 'yellow',
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    color: 'yellow',
    backgroundColor: 'black',
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));

export default function PersistentDrawerLeft(props) {
  const classes = useStyles();
  const { open, close } = props;
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton className={classes.headerIcon} onClick={close}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>
          <ListItem onClick={close} button component={Link} to="/">
            <ListItemIcon>
              <SearchIcon style={{ color: 'yellow' }} />
            </ListItemIcon>
            <ListItemText primary="Search" />
          </ListItem>
          <ListItem onClick={close} button component={Link} to="people">
            <ListItemIcon>
              <img src={People} alt="People" width="32" />
            </ListItemIcon>
            <ListItemText primary="People" />
          </ListItem>
          <ListItem onClick={close} button component={Link} to="planets">
            <ListItemIcon>
              <img src={DeathStar} alt="Planets " width="32" />
            </ListItemIcon>
            <ListItemText primary="Planets" />
          </ListItem>
          <ListItem onClick={close} button component={Link} to="films">
            <ListItemIcon>
              <img src={DarthVader} alt="Films" width="32" />
            </ListItemIcon>
            <ListItemText primary="Films" />
          </ListItem>
          <ListItem onClick={close} button component={Link} to="species">
            <ListItemIcon>
              <img src={BB8} alt="Species" width="32" />
            </ListItemIcon>
            <ListItemText primary="Species" />
          </ListItem>
          <ListItem onClick={close} button component={Link} to="vehicles">
            <ListItemIcon>
              <img src={Vehicle} alt="Vehicles" width="32" />
            </ListItemIcon>
            <ListItemText primary="Vehicles" />
          </ListItem>
          <ListItem onClick={close} button component={Link} to="starships">
            <ListItemIcon>
              <img src={Falcon} alt="Starships" width="32" />
            </ListItemIcon>
            <ListItemText primary="Starships" />
          </ListItem>
        </List>
        <Divider />
      </Drawer>
    </div>
  );
}
