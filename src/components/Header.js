import React, { Fragment, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { AuthContext } from '../contexts/AuthContext';
import logo from '../logo.svg';
import Drawer from './Drawer';
import history from '../history';
import app from '../config';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: 'black',
    color: 'yellow',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function Header() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const { user } = useContext(AuthContext);

  const guest = (
    <Fragment>
      <img src={logo} alt="logo" width="40" />
      <Typography variant="h6" className={classes.title}>
        Star Wars Fan App
      </Typography>
    </Fragment>
  );

  const logout = () => {
    app.auth().signOut();
    history.push('/');
  };

  function handleDrawerClose() {
    setOpen(false);
  }
  function handleDrawerOpen() {
    setOpen(true);
  }

  const authUser = (
    <Fragment>
      <IconButton
        color="inherit"
        aria-label="open drawer"
        onClick={handleDrawerOpen}
        edge="start"
        className={classes.menuButton}
      >
        <MenuIcon />
      </IconButton>
      <Typography variant="h6" className={classes.title}>
        Star Wars Fan App
      </Typography>
      <IconButton
        color="inherit"
        aria-label="logout"
        onClick={logout}
        edge="end"
      >
        <ExitToAppIcon />
      </IconButton>
    </Fragment>
  );

  return (
    <div>
      <AppBar className={classes.root} position="static">
        <Toolbar>
          <Fragment>{!user ? guest : authUser}</Fragment>
        </Toolbar>
      </AppBar>
      {user ? <Drawer open={open} close={handleDrawerClose} /> : null}
    </div>
  );
}
