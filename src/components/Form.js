import React from 'react';
// Styling
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';

export default function Form(props) {
  const useStyles = makeStyles(() => ({
    container: {
      padding: 25,
      background: 'linear-gradient(yellow, black)',
      textAlign: 'center',
      borderRadius: 5,
    },
    form: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'center',
      marginBottom: 30,
    },
    textField: {
      width: '-webkit-fill-available',
      backgroundColor: '#ffffff',
    },
    button: {
      marginBottom: 10,
    },
  }));
  const classes = useStyles();
  // eslint-disable-next-line react/prop-types
  const { children, onSubmit } = props;
  return (
    <Container className={classes.container}>
      <form className={classes.form} noValidate autoComplete="off">
        {children}
      </form>
      <Button
        onClick={onSubmit}
        className={classes.button}
        variant="contained"
        style={{ backgroundColor: 'black', color: 'yellow' }}
        size="large"
      >
        Submit
      </Button>
    </Container>
  );
}
