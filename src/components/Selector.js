/* eslint-disable react/prop-types */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    marginBottom: theme.spacing(6),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  underline: {
    '&:after': {
      borderBottom: `2px solid yellow`,
    },
  },
}));

export default function Ressources(props) {
  const classes = useStyles();
  const {
    selectValue,
    onChange,
    menuValue1,
    menuValue2,
    menuValue3,
    menuValue4,
    menuValue5,
    menuValue6,
  } = props;
  return (
    <FormControl required className={classes.formControl}>
      <InputLabel htmlFor="ressource-required">Ressources</InputLabel>
      <Select
        value={selectValue}
        onChange={onChange}
        name="ressource"
        inputProps={{
          id: 'ressource-required',
        }}
        className={classes.selectEmpty}
        input={
          <Input
            classes={{
              underline: classes.underline,
            }}
            name="age"
            id="age-helper"
          />
        }
      >
        <MenuItem value={menuValue1}>People</MenuItem>
        <MenuItem value={menuValue2}>Planets</MenuItem>
        <MenuItem value={menuValue3}>Films</MenuItem>
        <MenuItem value={menuValue4}>Species</MenuItem>
        <MenuItem value={menuValue5}>Vehicles</MenuItem>
        <MenuItem value={menuValue6}>Starships</MenuItem>
      </Select>
    </FormControl>
  );
}
