/* eslint-disable no-lone-blocks */
/* eslint-disable react/prop-types */
import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { AuthContext } from '../contexts/AuthContext';

{
  /* Wrapper around regular Routes. It passes the reste of the props, and than in our render function depending on the fact if we have user or not we redirect to the component or to the initial home route. */
}
const PrivateRoute = ({ component: RouteComponent, ...rest }) => {
  const { user } = useContext(AuthContext);
  return (
    <Route
      {...rest}
      render={routeProps =>
        // eslint-disable-next-line no-extra-boolean-cast
        !!user ? <RouteComponent {...routeProps} /> : <Redirect to="/login" />
      }
    />
  );
};

export default PrivateRoute;
