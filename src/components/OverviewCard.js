/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ItemDetails from './ItemDetails';

const useStyles = makeStyles({
  card: {
    minWidth: 275,
    textAlign: 'left',
    margin: 10,
    background: 'linear-gradient(0.3turn, black, red, black)',
  },
  text: {
    color: 'yellow',
  },
});

export default function OverviewCard(props) {
  const classes = useStyles();
  const { title, subtitle, details } = props;
  const [open, setOpen] = useState(false);

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }
  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography
          classes={{ colorTextPrimary: classes.text }}
          variant="h5"
          color="textPrimary"
          gutterBottom
        >
          {title}
        </Typography>
        <Typography
          classes={{ colorTextPrimary: classes.text }}
          variant="body2"
          color="textPrimary"
        >
          {subtitle}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" onClick={handleClickOpen}>
          <Typography
            classes={{ colorTextPrimary: classes.text }}
            variant="body2"
            color="textPrimary"
          >
            More Info
          </Typography>
        </Button>
        <ItemDetails
          close={handleClose}
          open={open}
          title={title}
          details={details}
        />
      </CardActions>
    </Card>
  );
}
