/* eslint-disable react/prop-types */
import React, { useEffect, useContext } from 'react';
import uuid from 'uuid/v4';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { DataContext } from '../contexts/DataContext';
import Loader from '../components/Loader';
import OverviewCard from '../components/OverviewCard';

const useStyles = makeStyles(() => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
}));

export default function List(props) {
  const classes = useStyles();
  const { fetch, data, loading } = useContext(DataContext);
  const { api } = props;
  useEffect(() => {
    fetch(api);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return !loading ? (
    <Container className={classes.container} maxWidth="lg">
      {data.map(item => (
        <OverviewCard
          title={item.name || item.title}
          subtitle={item.producer}
          key={uuid()}
          details={item}
        />
      ))}
    </Container>
  ) : (
    <Loader />
  );
}
