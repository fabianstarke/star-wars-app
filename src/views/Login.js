/* eslint-disable react/prop-types */
import React, { useState, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Redirect } from 'react-router-dom';
import Form from '../components/Form';
import FormTextField from '../components/FormTextField';
import { AuthContext } from '../contexts/AuthContext';
import app from '../config';

export default function Login({ history }) {
  const useStyles = makeStyles(theme => ({
    mainContainer: {
      marginTop: '50px',
      textAlign: 'center',
      alignItems: 'center',
      display: 'flex',
      flexDirection: 'row',
      [theme.breakpoints.down('xs')]: {
        flexWrap: 'wrap',
      },
    },
    titleContainer: {
      marginTop: '50px',
      maxWidth: 650,
    },
    formContainer: {
      display: 'flex',
      flexDirection: 'column',
    },
    h1: {
      textAlign: 'left',
      color: 'yellow',
    },
  }));

  const classes = useStyles();

  const [value, setValue] = useState({
    email: '',
    password: '',
  });

  const handleChange = title => e => {
    setValue({ ...value, [title]: e.target.value });
  };

  // Here we use useCallback hook to memorize our callback

  const SubmitLogin = useCallback(
    async event => {
      event.preventDefault();
      const { email, password } = value;
      try {
        await app.auth().signInWithEmailAndPassword(email, password);
        history.push('/');
      } catch (error) {
        alert(error);
      }
    },
    [history, value]
  );

  const { user } = useContext(AuthContext);
  if (user) {
    return <Redirect to="/" />;
  }

  // eslint-disable-next-line react/prop-types
  return (
    <div>
      <Container className={classes.titleContainer}>
        <h1 className={classes.h1}>LOGIN</h1>
        <Container className={classes.mainContainer}>
          <Container className={classes.formContainer}>
            <Form onSubmit={SubmitLogin} buttonName="LOGIN">
              <FormTextField
                id="email"
                label="Email"
                value={value.email}
                onChange={handleChange('email')}
              />
              <FormTextField
                id="password"
                label="Password"
                value={value.password}
                onChange={handleChange('password')}
              />
            </Form>
          </Container>
        </Container>
      </Container>
    </div>
  );
}
