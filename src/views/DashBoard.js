import React, { useState } from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import uuid from 'uuid';
import { debounce } from 'lodash';
import Selector from '../components/Selector';
import SearchBox from '../components/SearchBox';
import OverviewCard from '../components/OverviewCard';

const useStyles = makeStyles(theme => ({
  root: {
    textAlign: '-webkit-center',
    padding: 20,
  },
  button: {
    margin: theme.spacing(1),
  },
}));

export default function Dashboard() {
  const classes = useStyles();
 
  // State and setter for search result
  const [ressources, setRessources] = useState([]);
  // State and setter for search term
  const [values, setValues] = useState({
    ressource: '',
  });

  const handleChange = event => {
    setValues(oldValues => ({
      ...oldValues,
      [event.target.name]: event.target.value,
    }));
  };

  const search = q => {
    const url = `https://swapi.dev/api/${values.ressource}?search=${q}`;
    axios
      .get(url)
      .then(res => {
        if (q !== '') {
          setRessources(res.data.results);
        } else setRessources([]);
      })
      .catch(err => console.log(err));
  };
  const onChange = debounce(searchTerm => {
    search(searchTerm);
  }, 1000);

  return (
    <div className={classes.root}>
      <h2>Search for any Star Wars ressource you want to</h2>
      <Selector
        selectValue={values.ressource}
        onChange={handleChange}
        menuValue1="people"
        menuValue2="planets"
        menuValue3="films"
        menuValue4="species"
        menuValue5="vehicles"
        menuValue6="starships"
      />
      {values.ressource && (
        <SearchBox
          onChange={e => onChange(e.target.value)}
          placeholder={`Search for ${values.ressource}...`}
        />
      )}
      <Container maxWidth="sm">
        {ressources.map(ressource => (
          <OverviewCard
            title={ressource.title || ressource.name}
            subtitle={ressource.producer}
            key={uuid()}
            details={ressource}
          />
        ))}
      </Container>
    </div>
  );
}
