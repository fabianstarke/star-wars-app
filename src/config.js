import * as firebase from 'firebase/app';
import 'firebase/auth';

const app = firebase.initializeApp({
  apiKey: 'AIzaSyDCUsPdNJvMCEs734d4o5hRZXlhdc9gzYk',
  authDomain: 'star-wars-fan-app.firebaseapp.com',
  databaseURL: 'https://star-wars-fan-app.firebaseio.com',
  projectId: 'star-wars-fan-app',
  storageBucket: '',
  messagingSenderId: '303734910824',
  appId: '1:303734910824:web:3b586b9357d1d349',
});

export default app;
