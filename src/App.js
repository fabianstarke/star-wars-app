import React from 'react';
import { Router, Route } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Header from './components/Header';
import Login from './views/Login';
import Dashboard from './views/DashBoard';
import DataContextProvider from './contexts/DataContext';
import AuthContextProvider from './contexts/AuthContext';
import List from './views/List';
import PrivateRoute from './components/PrivateRoute';
import history from './history';

const useStyles = makeStyles(() => ({
  app: {
    textAlign: 'center',
    background: 'linear-gradient(black, yellow, black)',
    minHeight: '100vh',
  },
}));

const ListItems = [
  'people',
  'planets',
  'films',
  'species',
  'vehicles',
  'starships',
];

function App() {
  const classes = useStyles();

  return (
    <AuthContextProvider>
      <Router history={history}>
        <div className={classes.app}>
          <Header />
          <PrivateRoute exact path="/" component={Dashboard} />
          <Route exact path="/login" component={Login} />
          {/* Passing down the provider with the data */}
          <DataContextProvider>
            {/* passing the component an inline function that creates the element would work but according to the docs not the best solution as it affects performance. Instead of using Routes component prop, I use its render prop passing it an inline function then pass along the arguments to the element I am creating. */}
            {ListItems.map(list => (
              <Route
                key={list}
                path={`/${list}`}
                render={props => <List {...props} api={list} />}
              />
            ))}
          </DataContextProvider>
        </div>
      </Router>
    </AuthContextProvider>
  );
}

export default App;
