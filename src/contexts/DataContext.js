/* eslint-disable react/prop-types */
import React, { useState, createContext } from 'react';
import axios from 'axios';

export const DataContext = createContext();

export default function DataContextProvider(props) {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  const fetch = type => {
    const url = `https://swapi.dev/api/${type}`;
    axios
      .get(url)
      .then(res => {
        setData(res.data.results);
        setLoading(false);
      })
      .catch(err => console.log(err));
  };

  const { children } = props;
  return (
    <DataContext.Provider value={{ data, loading, fetch }}>
      {children}
    </DataContext.Provider>
  );
}
