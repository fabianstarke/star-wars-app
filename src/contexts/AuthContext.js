/* eslint-disable react/prop-types */
import React, { useState, createContext, useEffect } from 'react';
import app from '../config';

export const AuthContext = createContext();

export default function AuthContextProvider(props) {
  // const [auth, setAuth] = useState(false);
  const [user, setUser] = useState(null);

  useEffect(() => {
    app.auth().onAuthStateChanged(setUser);
  }, []);

  // Update user state evry time our authentification status changes in firebase

  const { children } = props;
  return (
    <AuthContext.Provider value={{ user }}>{children}</AuthContext.Provider>
  );
}
